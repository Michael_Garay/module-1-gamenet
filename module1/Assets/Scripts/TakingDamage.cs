﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakingDamage : MonoBehaviourPunCallbacks
{

    [SerializeField]
    Image healthbar;

    private float starthealth = 100;

    private float health;

    // Start is called before the first frame update
    void Start()
    {

        health = starthealth;
        healthbar.fillAmount = health / starthealth;

    }

    [PunRPC]
    public void TakeDamage(int damage)
    {
        health -= damage;
        Debug.Log(health);

        healthbar.fillAmount = health / starthealth;

        if (health < 0)
        {
            Die();
        }
    }

    private void Die()
    {
        if(photonView.IsMine)
        {
          GameManager.instance.LeaveRoom();
        }
        
    }
}
